package com.example.tp2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Set;

public class HighScore extends AppCompatActivity {
    private ListView listeViewHighScore;
    private ArrayList arrayList;
    private Button buttonMainMenu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_high_score);
        buttonMainMenu = (Button) findViewById(R.id.buttonMainMenu);
        listeViewHighScore = (ListView) findViewById(R.id.listeViewHighScore);
        SharedPreferences sharedPreferencesScore = getSharedPreferences("data", MODE_PRIVATE);

        arrayList = readSharedPreferences(sharedPreferencesScore);

        ArrayAdapter arrayAdapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, arrayList);

        listeViewHighScore.setAdapter(arrayAdapter);

        buttonMainMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                Intent intentGame = new Intent(v.getContext(), MainActivity.class);
                startActivity(intentGame);
            }
        });

        //DELETE GAMES IN HIGHSCORES
        listeViewHighScore.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                arrayList.remove(position);
                arrayAdapter.notifyDataSetChanged();
                return false;
            }
        });


    }

    private ArrayList<String> readSharedPreferences(SharedPreferences sharedPreferencesScore){
        ArrayList<String> arrayListTmp = new ArrayList<>();
        Set<String> stringsSet = sharedPreferencesScore.getStringSet("HighScore", null);
        if(stringsSet != null){
            arrayListTmp = new ArrayList<String>(stringsSet);
        }
        return arrayListTmp;
    }

}