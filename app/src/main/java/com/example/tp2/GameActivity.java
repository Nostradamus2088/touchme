package com.example.tp2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class GameActivity extends AppCompatActivity {

    private Button buttonQuit;
    private Button buttonDifficultyBeginner;
    private Button buttonDifficultyNormal;
    private Button buttonDifficultyExpert;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        buttonQuit = (Button) findViewById(R.id.buttonQuit);
        buttonDifficultyBeginner = (Button)findViewById(R.id.buttonDifficultyBeginner);
        buttonDifficultyNormal = (Button)findViewById(R.id.buttonDifficultyNormal);
        buttonDifficultyExpert = (Button)findViewById(R.id.buttonDifficultyExpert);

        buttonDifficultyBeginner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentGame = new Intent(v.getContext(), GameBeginner.class);
                startActivity(intentGame);
            }
        });

        buttonDifficultyNormal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentGame = new Intent(v.getContext(), GameMedium.class);
                startActivity(intentGame);
            }
        });

        buttonDifficultyExpert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentGame = new Intent(v.getContext(), GameHard.class);
                startActivity(intentGame);
            }
        });

        buttonQuit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}