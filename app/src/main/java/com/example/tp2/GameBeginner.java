package com.example.tp2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;
import java.util.TimeZone;
                                    //method vu sur stackoverflow
public class GameBeginner extends AppCompatActivity implements View.OnClickListener {

    private Button
            button1,
            button2,
            button3,
            button4,
            button5,
            button6,
            button7,
            button8,
            button9,
            buttonReady,
            buttonEndGame;

    private EditText editTextPersonName;

    private TextView
            textViewTime,
            textViewScore ;

    private int score = 0;

    private Handler hr;
    private Thread tr;

    private int timeLeft = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_beginner);

        //link buttons to variables
        buttonReady = (Button) findViewById(R.id.buttonReady);
        button1 = (Button) findViewById(R.id.button1);
        button2 = (Button) findViewById(R.id.button2);
        button3 = (Button) findViewById(R.id.button3);
        button4 = (Button) findViewById(R.id.button4);
        button5 = (Button) findViewById(R.id.button5);
        button6 = (Button) findViewById(R.id.button6);
        button7 = (Button) findViewById(R.id.button7);
        button8 = (Button) findViewById(R.id.button8);
        button9 = (Button) findViewById(R.id.button9);
        buttonEndGame = (Button) findViewById(R.id.buttonEndGame);

        //link edittext to the frontend
        editTextPersonName = (EditText) findViewById(R.id.editTextPersonName) ;

        //link les textView
        textViewScore = (TextView) findViewById(R.id.textViewScore);
        textViewTime = (TextView) findViewById(R.id.textViewTime);

        //l'action du bouton
        buttonReady.setOnClickListener(this);
        buttonEndGame.setOnClickListener(this);
    }

    //start the FIGHTTTTT
    public void fight()
    {
        show();
        textViewTime.setText("10");
        timeLeft=10;
        button1.setOnClickListener(this);
        button2.setOnClickListener(this);
        button3.setOnClickListener(this);
        button4.setOnClickListener(this);
        button5.setOnClickListener(this);
        button6.setOnClickListener(this);
        button7.setOnClickListener(this);
        button8.setOnClickListener(this);
        button9.setOnClickListener(this);


        button1.setVisibility(View.INVISIBLE);
        button2.setVisibility(View.INVISIBLE);
        button3.setVisibility(View.INVISIBLE);
        button4.setVisibility(View.INVISIBLE);
        button5.setVisibility(View.INVISIBLE);
        button6.setVisibility(View.INVISIBLE);
        button7.setVisibility(View.INVISIBLE);
        button8.setVisibility(View.INVISIBLE);
        button9.setVisibility(View.INVISIBLE);


        buttonReady.setEnabled(false);

        hr = new Handler();
        tr = new Thread(){
            @Override
            public void run()
            {
                hr.postDelayed(tr,1000);
                timeLeft--;
                textViewTime.setText(String.valueOf(timeLeft));
                if(timeLeft<=0)
                {
                    startGame();
                    hr.removeCallbacks(tr);
                };
            };
        };
        tr.run();
        show();
    }


    public void startGame()
    {

        textViewScore.setText(String.valueOf(score));
        button1.setOnClickListener(null);
        button2.setOnClickListener(null);
        button3.setOnClickListener(null);
        button4.setOnClickListener(null);
        button5.setOnClickListener(null);
        button6.setOnClickListener(null);
        button7.setOnClickListener(null);
        button8.setOnClickListener(null);
        button9.setOnClickListener(null);

        buttonReady.setEnabled(true);
        setVisibility();
    }

    //la method onClick qui refere a implements View.OnClickListener
    @Override
    public void onClick(View FIGHT)
    {
        switch(FIGHT.toString()){
            case "button1" : button1.setVisibility(View.INVISIBLE);
                break;
            case "button2" : button2.setVisibility(View.INVISIBLE);
                break;
            case "button3" : button3.setVisibility(View.INVISIBLE);
                break;
            case "button4" : button4.setVisibility(View.INVISIBLE);
                break;
            case "button5" : button5.setVisibility(View.INVISIBLE);
                break;
            case "button6" : button6.setVisibility(View.INVISIBLE);
                break;
            case "button7" : button7.setVisibility(View.INVISIBLE);
                break;
            case "button8" : button8.setVisibility(View.INVISIBLE);
                break;
            case "button9" : button9.setVisibility(View.INVISIBLE);
                break;
        }
        score++;
        show();
        //ajoute les points a chaque click dans le textview du score!
        textViewScore.setText(String.valueOf(score));

        //si on pese sur le bouton READY la fonction fight() es lancer
        // sinon le bouton ENd fini le match save les resultas du match dans une arraylist, lactivity finish() et on passe a la page highscore
        if(FIGHT== buttonReady)
        {
            fight();
        }
        else if(FIGHT ==  buttonEndGame){
            endMatch();
            finish();
            Intent intentGame = new Intent(FIGHT.getContext(), HighScore.class);
            startActivity(intentGame);
        }

    }

    //fonction pour faire un resultas random ALEATOIRE
    public int Randomize(int count)
    {
        int min =0 ;
        int max = 8;
        int randomize;
        do
        {
            randomize = new Random().nextInt((max-min) + 1) + min;
        }while(count == randomize);

        return randomize;
    }

    public void setVisibility()
    {
        //set la visibility a invisible
        button1.setVisibility(View.INVISIBLE);
        button2.setVisibility(View.INVISIBLE);
        button3.setVisibility(View.INVISIBLE);
        button4.setVisibility(View.INVISIBLE);
        button5.setVisibility(View.INVISIBLE);
        button6.setVisibility(View.INVISIBLE);
        button7.setVisibility(View.INVISIBLE);
        button8.setVisibility(View.INVISIBLE);
        button9.setVisibility(View.INVISIBLE);
    }

    public void show()
    {
        //Affiche les boutons avec la fonction RAMDOMIZE
        Random random = new Random();
        setVisibility();
        int randomInt = Randomize(0);
        switch(randomInt){
            case 1 : button1.setVisibility(View.VISIBLE);
            break;
            case 2 : button2.setVisibility(View.VISIBLE);
                break;
            case 3 : button3.setVisibility(View.VISIBLE);
                break;
            case 4 : button4.setVisibility(View.VISIBLE);
                break;
            case 5 : button5.setVisibility(View.VISIBLE);
                break;
            case 6 : button6.setVisibility(View.VISIBLE);
                break;
            case 7 : button7.setVisibility(View.VISIBLE);
                break;
            case 8 : button8.setVisibility(View.VISIBLE);
                break;
            case 9 : button9.setVisibility(View.VISIBLE);
                break;

        }

    }

    public void endMatch(){
        // 1. create et tu get linfo de sharedpreferences dans une arraylist
        // 2. add le score
        // 3. save and update arraylist avec les sharedpreferences dans la clef data
        ArrayList<String> arrayList;
        SharedPreferences sharedPreferencesScore;

        sharedPreferencesScore = getSharedPreferences("data", MODE_PRIVATE);
        arrayList = readSharedPreferences(sharedPreferencesScore);

        arrayList.add(editTextPersonName.getText().toString() + " "
                + textViewScore.getText().toString()
                + " Beginner difficulty"
                + " Date: " + getDate());
        updatedScoreList(arrayList, sharedPreferencesScore);
    }

    // fonction pour aller chercher la date!
    private String getDate()
    {
        Date date = new Date();
        SimpleDateFormat simpleDateFormat =  new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("GMT-4:00"));
        String dateToString = simpleDateFormat.format(date);
        return dateToString;
    }

    //fonction pour READSHAREDPREFERENCES
    private ArrayList<String> readSharedPreferences(SharedPreferences sharedPreferencesScore)
    {
        ArrayList<String> arrayListTmp = new ArrayList<>();
        Set<String> stringsSet = sharedPreferencesScore.getStringSet("HighScore", null);
        if(stringsSet != null){
            arrayListTmp = new ArrayList<String>(stringsSet);
        }
        return arrayListTmp;
    }

    // update le arraylist contenant les sharedpreferences du telephone
    private void updatedScoreList(ArrayList<String> list, SharedPreferences sharedPreferencesScore)
    {
        try {
            SharedPreferences.Editor editor = sharedPreferencesScore.edit();
            Set<String> setString = new HashSet<>();
            setString.addAll(list);
            editor.putStringSet("HighScore",setString);
            editor.commit();
        }catch (Exception e){
            Toast.makeText(GameBeginner.this, "ERROR", Toast.LENGTH_SHORT).show();
        }
    }
}