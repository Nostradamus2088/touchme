package com.example.tp2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;
import java.util.TimeZone;

public class GameHard extends AppCompatActivity implements View.OnClickListener{

    private Button
            button25,
            button26,
            button27,
            button28,
            button29,
            button30,
            button31,
            button32,
            button33,
            button34,
            button35,
            button36,
            button37,
            button38,
            button39,
            button40,
            buttonReadyH,
            buttonEndGameH;

    private EditText editTextPersonNameH;

    private TextView
            textViewTimeH,
            textViewScoreH ;

    private int score = 0;

    private Handler hr;
    private Thread tr;

    private int timeLeft = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_hard);

        buttonReadyH = (Button) findViewById(R.id.buttonReadyH);
        buttonEndGameH = (Button) findViewById(R.id.buttonEndGameH);
        button25 = (Button) findViewById(R.id.button25);
        button26 = (Button) findViewById(R.id.button26);
        button27 = (Button) findViewById(R.id.button27);
        button28 = (Button) findViewById(R.id.button28);
        button29 = (Button) findViewById(R.id.button29);
        button30 = (Button) findViewById(R.id.button30);
        button31 = (Button) findViewById(R.id.button31);
        button32 = (Button) findViewById(R.id.button32);
        button33 = (Button) findViewById(R.id.button33);
        button34 = (Button) findViewById(R.id.button34);
        button35 = (Button) findViewById(R.id.button35);
        button36 = (Button) findViewById(R.id.button36);
        button37 = (Button) findViewById(R.id.button37);
        button38 = (Button) findViewById(R.id.button38);
        button39 = (Button) findViewById(R.id.button39);
        button40 = (Button) findViewById(R.id.button40);

        //link edittext to the frontend
        editTextPersonNameH = (EditText) findViewById(R.id.editTextPersonNameH) ;

        //link les textView
        textViewScoreH = (TextView) findViewById(R.id.textViewScoreH);
        textViewTimeH = (TextView) findViewById(R.id.textViewTimeM);

        //l'action du bouton
        buttonReadyH.setOnClickListener(this);
        buttonEndGameH.setOnClickListener(this);
    }

    public void fight()
    {
        show();
        textViewTimeH.setText("10");
        timeLeft=10;
        button25.setOnClickListener(this);
        button26.setOnClickListener(this);
        button27.setOnClickListener(this);
        button28.setOnClickListener(this);
        button29.setOnClickListener(this);
        button30.setOnClickListener(this);
        button31.setOnClickListener(this);
        button32.setOnClickListener(this);
        button33.setOnClickListener(this);
        button34.setOnClickListener(this);
        button35.setOnClickListener(this);
        button36.setOnClickListener(this);
        button37.setOnClickListener(this);
        button38.setOnClickListener(this);
        button39.setOnClickListener(this);
        button40.setOnClickListener(this);


        button25.setVisibility(View.INVISIBLE);
        button26.setVisibility(View.INVISIBLE);
        button27.setVisibility(View.INVISIBLE);
        button28.setVisibility(View.INVISIBLE);
        button29.setVisibility(View.INVISIBLE);
        button30.setVisibility(View.INVISIBLE);
        button31.setVisibility(View.INVISIBLE);
        button32.setVisibility(View.INVISIBLE);
        button33.setVisibility(View.INVISIBLE);
        button34.setVisibility(View.INVISIBLE);
        button35.setVisibility(View.INVISIBLE);
        button36.setVisibility(View.INVISIBLE);
        button37.setVisibility(View.INVISIBLE);
        button38.setVisibility(View.INVISIBLE);
        button39.setVisibility(View.INVISIBLE);
        button40.setVisibility(View.INVISIBLE);


        buttonReadyH.setEnabled(false);

        hr = new Handler();
        tr = new Thread(){
            @Override
            public void run()
            {
                hr.postDelayed(tr,1000);
                timeLeft--;
                textViewTimeH.setText(String.valueOf(timeLeft));
                if(timeLeft<=0)
                {
                    startGame();
                    hr.removeCallbacks(tr);
                };
            };
        };
        tr.run();
        show();
    }
    public void startGame()
    {

        textViewScoreH.setText(String.valueOf(score));
        button25.setOnClickListener(null);
        button26.setOnClickListener(null);
        button27.setOnClickListener(null);
        button28.setOnClickListener(null);
        button29.setOnClickListener(null);
        button30.setOnClickListener(null);
        button31.setOnClickListener(null);
        button32.setOnClickListener(null);
        button33.setOnClickListener(null);
        button34.setOnClickListener(null);
        button35.setOnClickListener(null);
        button36.setOnClickListener(null);
        button37.setOnClickListener(null);
        button38.setOnClickListener(null);
        button39.setOnClickListener(null);
        button40.setOnClickListener(null);

        buttonReadyH.setEnabled(true);
        setVisibility();
    }


    @Override
    public void onClick(View FIGHT){
        switch(FIGHT.toString()){
            case "button25" : button25.setVisibility(View.INVISIBLE);
                break;
            case "button26" : button26.setVisibility(View.INVISIBLE);
                break;
            case "button27" : button27.setVisibility(View.INVISIBLE);
                break;
            case "button28" : button28.setVisibility(View.INVISIBLE);
                break;
            case "button29" : button29.setVisibility(View.INVISIBLE);
                break;
            case "button30" : button30.setVisibility(View.INVISIBLE);
                break;
            case "button31" : button31.setVisibility(View.INVISIBLE);
                break;
            case "button32" : button32.setVisibility(View.INVISIBLE);
                break;
            case "button33" : button33.setVisibility(View.INVISIBLE);
                break;
            case "button34" : button34.setVisibility(View.INVISIBLE);
                break;
            case "button35" : button35.setVisibility(View.INVISIBLE);
                break;
            case "button36" : button36.setVisibility(View.INVISIBLE);
                break;
            case "button37" : button37.setVisibility(View.INVISIBLE);
                break;
            case "button38" : button38.setVisibility(View.INVISIBLE);
                break;
            case "button39" : button39.setVisibility(View.INVISIBLE);
                break;
            case "button40" : button40.setVisibility(View.INVISIBLE);
                break;
        }
        score++;
        show();
        //ajoute les points a chaque click dans le textview du score!
        textViewScoreH.setText(String.valueOf(score));

        //si on pese sur le bouton READY la fonction fight() es lancer
        // sinon le bouton ENd fini le match save les resultas du match dans une arraylist, lactivity finish() et on passe a la page highscore
        if(FIGHT== buttonReadyH)
        {
            fight();
        }
        else if(FIGHT ==  buttonEndGameH){
            endMatchH();
            finish();
            Intent intentGame = new Intent(FIGHT.getContext(), HighScore.class);
            startActivity(intentGame);
        }
    }


    //fonction pour faire un resultas random ALEATOIRE
    public int Randomize(int count)
    {
        int min =0 ;
        int max = 15;
        int randomize;
        do
        {
            randomize = new Random().nextInt((max-min) + 1) + min;
        }while(count == randomize);

        return randomize;
    }

    public void setVisibility()
    {
        //set la visibility a invisible
        button25.setVisibility(View.INVISIBLE);
        button26.setVisibility(View.INVISIBLE);
        button27.setVisibility(View.INVISIBLE);
        button28.setVisibility(View.INVISIBLE);
        button29.setVisibility(View.INVISIBLE);
        button30.setVisibility(View.INVISIBLE);
        button31.setVisibility(View.INVISIBLE);
        button32.setVisibility(View.INVISIBLE);
        button33.setVisibility(View.INVISIBLE);
        button34.setVisibility(View.INVISIBLE);
        button35.setVisibility(View.INVISIBLE);
        button36.setVisibility(View.INVISIBLE);
        button37.setVisibility(View.INVISIBLE);
        button38.setVisibility(View.INVISIBLE);
        button39.setVisibility(View.INVISIBLE);
        button40.setVisibility(View.INVISIBLE);
    }

    public void show()
    {
        //Affiche les boutons avec la fonction RAMDOMIZE
        Random random = new Random();
        setVisibility();
        int randomInt = Randomize(0);
        switch(randomInt){
            case 1 : button25.setVisibility(View.VISIBLE);
                break;
            case 2 : button26.setVisibility(View.VISIBLE);
                break;
            case 3 : button27.setVisibility(View.VISIBLE);
                break;
            case 4 : button28.setVisibility(View.VISIBLE);
                break;
            case 5 : button29.setVisibility(View.VISIBLE);
                break;
            case 6 : button30.setVisibility(View.VISIBLE);
                break;
            case 7 : button31.setVisibility(View.VISIBLE);
                break;
            case 8 : button32.setVisibility(View.VISIBLE);
                break;
            case 9 : button33.setVisibility(View.VISIBLE);
                break;
            case 10 : button34.setVisibility(View.VISIBLE);
                break;
            case 11 : button35.setVisibility(View.VISIBLE);
                break;
            case 12 : button36.setVisibility(View.VISIBLE);
                break;
            case 13 : button37.setVisibility(View.VISIBLE);
                break;
            case 14 : button38.setVisibility(View.VISIBLE);
                break;
            case 15 : button39.setVisibility(View.VISIBLE);
                break;
            case 16 : button40.setVisibility(View.VISIBLE);
                break;

        }

    };

    public void endMatchH(){
        // 1. create et tu get linfo de sharedpreferences dans une arraylist
        // 2. add le score
        // 3. save and update arraylist avec les sharedpreferences dans la clef data
        ArrayList<String> arrayList;
        SharedPreferences sharedPreferencesScore;

        sharedPreferencesScore = getSharedPreferences("data", MODE_PRIVATE);
        arrayList = readSharedPreferences(sharedPreferencesScore);

        arrayList.add(editTextPersonNameH.getText().toString() + " "
                + textViewScoreH.getText().toString()
                + "  Hard difficulty"
                + " Date: " + getDate());
        updatedScoreList(arrayList, sharedPreferencesScore);
    }

    // fonction pour aller chercher la date!
    private String getDate(){
        Date date = new Date();
        SimpleDateFormat simpleDateFormat =  new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("GMT-4:00"));
        String dateToString = simpleDateFormat.format(date);
        return dateToString;
    }

    //fonction pour READSHAREDPREFERENCES
    private ArrayList<String> readSharedPreferences(SharedPreferences sharedPreferencesScore){
        ArrayList<String> arrayListTmp = new ArrayList<>();
        Set<String> stringsSet = sharedPreferencesScore.getStringSet("HighScore", null);
        if(stringsSet != null){
            arrayListTmp = new ArrayList<String>(stringsSet);
        }
        return arrayListTmp;
    }

    // update le arraylist contenant les sharedpreferences du telephone
    private void updatedScoreList(ArrayList<String> list, SharedPreferences sharedPreferencesScore){
        try {
            SharedPreferences.Editor editor = sharedPreferencesScore.edit();
            Set<String> setString = new HashSet<>();
            setString.addAll(list);
            editor.putStringSet("HighScore",setString);
            editor.commit();
        }catch (Exception e){
            //Toast.makeText(GameBeginner.this, "ERROR", Toast.LENGTH_SHORT).show();
        }
    }

}